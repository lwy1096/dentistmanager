package com.example.denticecustomermanager.controller;

import com.example.denticecustomermanager.model.DentistCustomerItem;
import com.example.denticecustomermanager.model.DentistCustomerRequest;
import com.example.denticecustomermanager.model.DentistCustomerUpdateRequest;
import com.example.denticecustomermanager.service.DenistManageService;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/dentist")
public class DentistCunstomerManageController {
    private final DenistManageService denistManageService;
    @PostMapping("/customer")
    public String setCustomermanager(@RequestBody @Validated DentistCustomerRequest request){
        denistManageService.setCustomerManager(request);
        return "Thanks for Writing";
    }
    @GetMapping("/data")
    public List<DentistCustomerItem> getCustomer() {
        List<DentistCustomerItem> result = denistManageService.getCustomerData();

        return result;
    }
    @PutMapping("/add-data/id/{id}")
    public String putCustomerData(@PathVariable long id,@RequestBody @Validated DentistCustomerUpdateRequest request) {
        denistManageService.putCustomer(id, request);

        return "Thanks for writing";
    }
}
