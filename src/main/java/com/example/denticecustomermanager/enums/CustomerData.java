package com.example.denticecustomermanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CustomerData {
    WAIT("대기"),
    NEED_CHECK("검진필요"),
    FREEZE("휴면");
    private final String status;

}
