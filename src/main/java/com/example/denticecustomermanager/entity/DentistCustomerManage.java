package com.example.denticecustomermanager.entity;

import com.example.denticecustomermanager.enums.CustomerData;
import com.example.denticecustomermanager.enums.Gender;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class DentistCustomerManage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 15)
    private String name;
    @Column(nullable = false, length = 5)
    @Enumerated(value = EnumType.STRING)
    private Gender gender;

    @Enumerated(value = EnumType.STRING)
    private CustomerData customerData;
    @Column(nullable = false,length = 15)
    private String idNum;


    @Column(nullable = false)
    private String address;

    private String purpose;

    private String ect;

    private String allergy;

    @Column(nullable = false)
    private LocalDateTime firstEnter;


    private  LocalDate nextReserve;
    private Integer daysRequest;
    private String previousVisit;
}
