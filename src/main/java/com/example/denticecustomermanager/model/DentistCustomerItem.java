package com.example.denticecustomermanager.model;

import com.example.denticecustomermanager.enums.CustomerData;
import com.example.denticecustomermanager.enums.Gender;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class DentistCustomerItem {
    private Long id;
    private String name;
    @Enumerated(value = EnumType.STRING)
    @Length(min=3, max= 6)
    private Gender gender;
    private String idNum;
    private String address;
    private String purpose;
    private String ect;
    private String allergy;
    @Enumerated(value = EnumType.STRING)
    @Length(min= 2, max = 15)
    private CustomerData customerData;
    private LocalDateTime nextReserve;

    private Integer daysRequest;
    private String previousVisitDays;
}
