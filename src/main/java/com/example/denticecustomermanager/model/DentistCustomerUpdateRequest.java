package com.example.denticecustomermanager.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class DentistCustomerUpdateRequest {
    private String purpose;
    private Integer daysRequest;
}
