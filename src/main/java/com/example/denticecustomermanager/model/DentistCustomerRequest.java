package com.example.denticecustomermanager.model;

import com.example.denticecustomermanager.enums.Gender;
import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Getter
@Setter
public class DentistCustomerRequest {
    @NotNull
    @Length(min=2, max =15)
    private String name;
    @NotNull
    private String idNum;

    @Enumerated(value = EnumType.STRING)
    private Gender gender;
    @NotNull
    private String address;

    private String purpose;
    private String ect;
    private String allergy;
    private Integer daysRequest;

}
