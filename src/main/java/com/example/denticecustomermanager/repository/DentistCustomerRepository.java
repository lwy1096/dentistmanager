package com.example.denticecustomermanager.repository;

import com.example.denticecustomermanager.entity.DentistCustomerManage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DentistCustomerRepository extends JpaRepository<DentistCustomerManage, Long> {
}
