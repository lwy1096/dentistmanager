package com.example.denticecustomermanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DenticeCustomerManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(DenticeCustomerManagerApplication.class, args);
    }

}
