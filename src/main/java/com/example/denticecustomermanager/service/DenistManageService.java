package com.example.denticecustomermanager.service;

import com.example.denticecustomermanager.entity.DentistCustomerManage;
import com.example.denticecustomermanager.enums.CustomerData;
import com.example.denticecustomermanager.model.DentistCustomerItem;
import com.example.denticecustomermanager.model.DentistCustomerRequest;
import com.example.denticecustomermanager.model.DentistCustomerUpdateRequest;
import com.example.denticecustomermanager.repository.DentistCustomerRepository;
import lombok.RequiredArgsConstructor;
import org.hibernate.validator.internal.util.privilegedactions.LoadClass;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DenistManageService {
    private final DentistCustomerRepository dentistCustomerRepository;
    public void setCustomerManager(DentistCustomerRequest request) {
        DentistCustomerManage addData = new DentistCustomerManage();
        addData.setName(request.getName());
        addData.setIdNum(request.getIdNum());
        addData.setAddress(request.getAddress());
        addData.setGender(request.getGender());
        addData.setEct(request.getEct());
        addData.setAllergy(request.getAllergy());
        addData.setFirstEnter(LocalDateTime.now());
        addData.setPurpose(request.getPurpose());
        dentistCustomerRepository.save(addData);
    }
    public List<DentistCustomerItem> getCustomerData() {
        List<DentistCustomerItem> result = new LinkedList<>();
        List<DentistCustomerManage> originData= dentistCustomerRepository.findAll();
        for(DentistCustomerManage item: originData) {
            DentistCustomerItem addItem = new DentistCustomerItem();
            addItem.setId(item.getId());
            addItem.setName(item.getName());
            addItem.setGender(item.getGender());
            addItem.setIdNum(item.getIdNum());
            addItem.setAddress(item.getAddress());
            addItem.setPurpose(item.getPurpose());
            addItem.setEct(item.getEct());
            addItem.setAllergy(item.getAllergy());

            // 일수 세기
            LocalDateTime startDate = LocalDateTime.of(
                    item.getFirstEnter().getYear(),
                    item.getFirstEnter().getMonthValue(),
                    item.getFirstEnter().getDayOfMonth(),0,0,0
            );
            LocalDateTime registeredTime = item.getFirstEnter();
            Long countDate = ChronoUnit.DAYS.between(registeredTime,startDate);
            addItem.setPreviousVisitDays(countDate + "일");
            //일수 카운트 이후 판별
            CustomerData customerData= CustomerData.FREEZE;
            if(countDate<=30) {
                customerData = CustomerData.WAIT;
            }else if (countDate<=180) {
                customerData = CustomerData.NEED_CHECK;
            }
            //요청일
            Integer reserveDays = item.getDaysRequest();
            addItem.setNextReserve((reserveDays==null? LocalDateTime.now().plusMonths(6):LocalDateTime.now().plusDays((reserveDays))));
            addItem.setCustomerData(customerData);
            result.add(addItem);
            }
        return result;
    }
    public void putCustomer(long id , DentistCustomerUpdateRequest request ) {
        DentistCustomerManage originData  = dentistCustomerRepository.findById(id).orElseThrow();
        Integer reserveDays = request.getDaysRequest();
        originData.setNextReserve(LocalDate.now().plusDays(reserveDays));
        originData.setPurpose(request.getPurpose());

    }
}
